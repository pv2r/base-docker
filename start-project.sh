echo "INICIANDO AMBIENTE DE DESENVOLVIMENTO LOCAL"
echo "==================="

echo "INICIANDO BANCO DE DADOS"
echo "==================="

cd base-docker
docker-compose down && docker-compose up -d  
echo "==================="
echo "INICIANDO CUSTODIA-BLOCKCHAIN"
echo "==================="

cd ../blockchain
docker-compose down && docker-compose up -d  

echo "==================="
echo "INICIANDO API-BLOCKCHAIN"
echo "==================="

cd ../api-blockchain
docker-compose down && docker-compose up -d  